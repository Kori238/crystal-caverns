class StatusEffects:
    health = magic = damage = max_health = pos = display_name = 0
    def death(self, player):
        pass
    def poisoned(self, player, isplayer):
        if self.max_health/10 < 1:
            damage = 1
        else:
            damage = round(self.max_health/10)
        if self.health - damage <= 0:
            if isplayer:
                print(f'You took {self.health} damage from poison and fainted!')
            else:
                print(f'{self.display_name} {str(self.pos+1)} took {self.health} damage from poison and fainted')
            self.health = 0
            self.death(player)
        else:
            if isplayer:
                print(f'You took {int(damage)} damage from poison.')
            else:
                print(f'{self.display_name} {str(self.pos+1)} took {int(damage)} damage from poison')
            self.health -= int(damage)
    def regeneration(self, player, isplayer):
        if self.max_health/10 < 1:
            heal = 1
        else:
            heal = round(self.max_health/10)
        if self.health + heal >= self.max_health:
            print(f'You restored {str(self.max_health - self.health)} from regeneration')
            self.health = self.max_health
        else:
            print(f'You restored {str(heal)} from regeneration')
            self.health += heal
    def vulnerable(self, player, isplayer):
        player.damage_taken_multiplier = 1.5
