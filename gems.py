from targeting import SingleTarget, MultiTarget, SpecialTarget, PlayerTarget
from random import randint, choice
class Attack(SingleTarget):
    def ability(self, player, target):
        if target.health - player.damage <= 0:
            print(f'You hit the {target.display_name} with {player.weapon.name} for {target.health} damage and it fainted!')
            target.health = 0
            target.death(player)
        else:
            print(f'You hit the {target.display_name} with {player.weapon.name} for {player.damage} damage.')
            target.health -= player.damage

class LimeGem(SingleTarget):
    name = 'limegem'
    display_name = 'Lime Gem'
    def ability(self, player, target):
        if player.magic <= 4:
            print('You don\'t have enough magic to do this, use a magic potion or wait for it to regenerate')
            return player.turn(False)
        else:
            player.magic -= 4
        if target.health - player.weapon.gem_damage <= 0:
            print(f'You hit the {target.display_name} with Lime Gem for {player.weapon.gem_damage} damage, inflicting poison for 3 turns and causing it to faint!')
            target.health = 0
            target.death(player)
        else:
            print(f'You hit the {target.display_name} with Lime Gem for {player.weapon.gem_damage} damage and inflicting poison for 3 turns')
            target.health -= player.weapon.gem_damage
            target.status_effects['poisoned'] = 3

class PinkGem(PlayerTarget):
    name = 'pinkgem'
    display_name = 'Pink Gem'
    def ability(self, player, target):
        if player.magic <= 5:
            print('You don\'t have enough magic to do this, use a magic potion or wait for it to regenerate')
            return player.turn(False)
        else:
            player.magic -= 5
        player.status_effects['regeneration'] = 4

class RedGem(PlayerTarget):
    name = 'redgem'
    display_name = 'Red Gem'
    def ability(self, player, target):
        if player.magic <= 4:
            print('You don\'t have enough magic to do this, use a magic potion or wait for it to regenerate')
            return player.turn(False)
        else:
            player.magic -= 4
        if player.health + round(player.max_health/3) >= player.max_health:
            print(f'You healed {str(player.max_health)} health')
            player.health = player.max_health
        else:
            print(f'You healed {str(round(player.max_health/3))} health')
            player.health += round(player.max_health/3)

class PurpleGem(SpecialTarget):
    name = 'purplegem'
    display_name = 'Purple Gem'
    def ability(self, player, enemies, targets):
        target = []
        for _ in range(0, player.weapon.gem_damage + 1):
            target.append(int(choice(targets))-1)
        if player.magic <= 6:
            print('You don\'t have enough magic to do this, use a magic potion or wait for it to regenerate')
            return player.turn(False)
        else:
            player.magic -= 6
        try:
            for pos in target:
                print(pos)
                if enemies[pos].health - 2 <= 0:
                    print(f'You dealt {enemies[pos].health} damage to {enemies[pos].display_name} with the Purple Gem and it fainted!')
                    enemies[pos].health = 0
                    enemies[pos].death(player)
                else:
                    print(f'You dealt 2 damage to {enemies[pos].display_name} with the Purple Gem') 
                    enemies[pos].health -= 2
        except AttributeError:
            pass

