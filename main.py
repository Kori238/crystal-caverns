# Crystal Caverns
# A text-based adventure game
# Jake Edwards
from gems import Attack, LimeGem, RedGem, PinkGem, PurpleGem
from enemies import GreenSnake
from status_effects import StatusEffects
from commands import Info, Go
from items import Potion, Bomb, Antidote
from sys import exit

info = player = None
go = Go()

intro = '''Welcome {name}! You're a curious and experienced miner/adventurer who recently heard of the legends of the Crystal Cavern in Dresdor.
Many miners have ventured into the caverns before you but none have returned. For some unknown reason you believe you can do better than all those before you...
As typical of adventure games... Prove to me and yourself that you can endure the dangers of the Crystal Cavern and wield the wealth or perhaps power of the hidden depths.

Through your adventures you will pick up Gems that allow you to defeat threats in different ways.
But, first you must choose what tool you shall use to harness the raw power in these vessels.

Throughout your adventure you will need to use commands to interact with the world around you. type 'help' for a list of commands'''

weapon_choice = '''
A)The Glass Sword:
A sword made from a sturdy glass with a container located in the hilt, perhaps it can hold something?
You will start with +5 max health and +1 damage

B)The Glass Stave
A long stick said to be enchanted by those of old, There is a contanier located on the top, perhaps it can hold something?
You will start with +5 max magic and +1 gem damage

(Whichever weapon you chose will impact what gems can do for you during your adventure)'''
limegem = LimeGem()
redgem = RedGem()
pinkgem = PinkGem()
purplegem = PurpleGem()

smallhealthpotion = Potion('small', 'health')
mediumhealthpotion = Potion('medium', 'health')
largehealthpotion = Potion('large', 'health')
smallmagicpotion = Potion('small', 'magic')
mediummagicpotion = Potion('medium', 'magic')
largemagicpotion = Potion('large', 'magic')
bomb = Bomb()
antidote = Antidote()
class Stave:
    damage = 1
    hp_bonus = 0
    mp_bonus = 5
    gem_damage = 2
    name = 'stave'
    display_name = 'The Glass Stave'

class Sword:
    damage = 2
    hp_bonus = 5
    mp_bonus = 0
    gem_damage = 1
    name = 'sword'
    display_name = 'The Glass Sword'
class Player(StatusEffects):
    health = magic = max_health = max_magic = 1000
    level = 0
    experience = 0
    gold = 50
    damage_taken_multiplier = 1
    enemies = [None, None, None]
    gems = []
    addgems = []
    additems = []
    items = {
        'smallhealthpotion': {
            'object': smallhealthpotion,
            'value': 0
        },
        'mediumhealthpotion': {
            'object': mediumhealthpotion,
            'value': 3
        },
        'largehealthpotion': {
            'object': largehealthpotion,
            'value': 0,
        },
        'smallmagicpotion': {
            'object': smallmagicpotion,
            'value': 0,
        },
        'mediummagicpotion': {
            'object': mediummagicpotion,
            'value': 0,
        },
        'largemagicpotion': {
            'object': largemagicpotion,
            'value': 0,
        },
        'antidote': {
            'object': antidote,
            'value': 0,
        },
        'bomb': {
            'object': bomb,
            'value': 0,
        }
    }
    value = 0
    status_effects = {}
    attack = Attack()
    position = 'forest6'
    next_position = farthest = farthestL = ''
    combat = False
    canleave = True

    def __init__(self, name, weapon):
        self.name = name
        if weapon == 'stave':
            self.weapon = Stave()
        elif weapon == 'sword':
            self.weapon = Sword()
        self.damage = self.weapon.damage
        self.gem_damage = self.weapon.gem_damage
        self.max_health = self.health = self.health + self.weapon.hp_bonus
        self.max_magic = self.magic = self.magic + self.weapon.mp_bonus

    def __getitem__(self, item):
        exec(f'self.value = self.{item}')
        return self.value

    def __setitem__(self, item, value):
        exec(f'self.{item} = {value}')

    def display(self):
        allign = 0
        try:
            if self.enemies[1].valid:
                allign += 50
            if self.enemies[2].valid:
                allign += 40
        except AttributeError:
            pass
        print((f'{self.name}:').center(allign))
        print((f'Health: {self.health}/{self.max_health}     Magic: {self.magic}/{self.max_magic}      Gold: {self.gold}').center(allign))
        print((f'Damage: {self.damage}                       Gem Damage: {self.gem_damage}').center(allign))
        if len(self.status_effects.items()) >= 1:
            message = ('Status Effects: ')
            for status_effect, length in self.status_effects.items():
                message += f'  {status_effect}: {str(length+1)}'
            print(message.center(allign))
        print('')

    def death(self):
        print('You\'ve fainted! You awake in your cabin with none of your items or gems..')
        print('You feel more experienced however.. perhaps you haven\'t lost everything')
        print('Thank you for playing! you can play again if you\'d like.')
        exit()

    def turn(self, status):
        to_delete = []              
        if status: 
            for key in self.status_effects:
                exec(f'self.{key}(self, True)')
                if self.status_effects[key] == 0:
                    to_delete.append(key)
                self.status_effects[key] -= 1
            for item in to_delete:
                if item == 'vulnerable':
                    player.damage_taken_multiplier = 1
                del self.status_effects[item]
        self.display()
        print('What would you like to do?')
        valid_options = ['a']
        choice = ''
        print('A) Attack')
        if len(self.gems) >= 1:
            print('B) Gems')
            valid_options.append('b')
        if len(self.items) >= 1:
            print('C) Items')
            valid_options.append('c')
        while choice not in valid_options:
            info.game_input(choice, False, player)
            choice = input('>').lower()
            
        if choice == 'a':
            self.attack.use(self, self.enemies)
        elif choice == 'b':
            i = 0
            valid_options = ['b']
            message = choice = ''
            for gem in self.gems:
                i += 1
                message += f'({i}) {gem.display_name: <20}'
                valid_options.append(str(i))
                if i == 5:
                    print(message)
                    message = ''
            temp_message = '(B) Back'
            print(message + f'{temp_message: <20}') 
            while choice not in valid_options:
                info.game_input(choice, False, player)
                choice = input('>').lower()
            if choice == 'b':
                return self.turn(False)
            else:
                self.gems[int(choice)-1].use(self, self.enemies)
        elif choice == 'c':
            i = 0
            valid_options = {
                'b': 'b',
            }
            message = choice = ''
            print('Which item would you like to use?')
            for item in self.items.keys():
                value = self.items[item]['value']
                item = self.items[item]['object']
                if value > 0:
                    i += 1
                    message += f'({i}) {item.display_name: <20}: {value}'
                    valid_options[str(i)] = item.name
                    if i == 5:
                        print(message)
                        message = ''
            temp_message = ' (B) Back'    
            print(message + f'{temp_message: <20}')
            while choice not in valid_options.keys():
                info.game_input(choice, False, player)
                choice = input('>').lower()
            if choice == 'b':
                return self.turn(False)
            else:
                self.items[valid_options[choice]]['object'].use(self, self.enemies)   

def end(player):
    player.magic = player.max_magic
    go.game_input(player.next_position, True, player)

def combat(player):
    valid = False
    for item in player.enemies:
        if item != None:
            valid = True
    if valid == False:
        return end(player)
    line1 = line2 = line3 = ''
    for enemy in player.enemies:
        try:
            line1_temp, line2_temp, line3_temp = enemy.display()
            line1 += line1_temp
            line2 += line2_temp
            line3 += line3_temp
        except AttributeError:
            pass
    print(line1 + '\n' + line2 + '\n' + line3)
    player.turn(True)
    for enemy in player.enemies:
        try:
            enemy.turn(player)
        except AttributeError:
            pass
    if player.magic != player.max_magic:
        print('You\'ve regenerated 1 magic')
        player.magic += 1
    return combat(player)

def start():
    global info, player
    name = confirm = weapon = ''
    while name == '' or confirm not in ['yes', 'y']:
        name = input('Welcome Adventurer! What would you like to be called?\n> ')
        confirm = input(f'Are you sure you would like to be called {name}?\n> ').lower()
    confirm = ''
    print(intro.format(name=name))
    while weapon not in ['sword', 'stave'] or confirm not in ['yes', 'y']:
        weapon = input(weapon_choice + '\n> ').lower()
        if weapon in ['a', 'sword', 'glass sword', 'the glass sword']:
            weapon = 'sword'
            confirm = input(f'Are you sure you would like to use The Glass Sword?\n> ').lower()
        elif weapon in ['b', 'stave', 'glass stave', 'the glass stave']:
            weapon = 'stave'
            confirm = input(f'Are you sure you would like to use The Glass Stave?\n> ').lower()
    player = Player(name, weapon)
    info = Info(player.weapon.name)
    print(f'You are now wielding {player.weapon.name}')
    return cabin()

def gameloop():
    global player
    choice = input('> ')
    go.game_input(choice, True, player)
    info.game_input(choice, True, player)
    try:
        for i in player.addgems:
            exec(f'{i}gem = {i[:1].upper() + i[1:]}Gem()')
            exec(f'player.gems.append({i}gem)')
        player.addgems = []
    except AttributeError:
        pass
    try:
        for i in player.additems:
            player.items[i]['value'] += 1
        player.additems = []
    except AttributeError:
        pass
    if player.combat:
        combat(player)
    return gameloop()

def cabin():
    global player
    print(f'You wake up in a small wooden cabin. You get dressed and pick up a few medium health potions and {player.weapon.display_name}')
    print(f'You can go outside')
    player.position = 'cabin'
    return gameloop()

start()