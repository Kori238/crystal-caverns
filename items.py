from targeting import SingleTarget, MultiTarget, SpecialTarget, PlayerTarget
class Potion(PlayerTarget):
    def __init__(self, size, stat):
        self.name = f'{size}{stat}potion'
        self.display_name = f'{size[:1].upper() + size[1:]} {stat[:1].upper() + stat[1:]} Potion'
        self.stat = stat
        if size == 'small':
            self.restore_amount = 5
        elif size == 'medium':
            self.restore_amount = 10
        elif size == 'large':
            self.restore_amount = 20
    def ability(self, player, target):
        if player.items[self.name]['value'] <= 0:
            print('You don\'t have any left! find or buy some more')
            return player.turn(False)
        elif player[self.stat] + self.restore_amount >= player['max_'+self.stat]:
            print('You restored ' + str(player['max_'+self.stat] - player[self.stat]) + ' ' + self.stat)
            player[self.stat] = player['max_'+self.stat]
        else:
            print('You restored ' + str(self.restore_amount) + ' ' + self.stat)
            player[self.stat] += self.restore_amount
        player.items[self.name]['value'] -= 1

class Bomb(MultiTarget):
    name = 'bomb'
    display_name = 'Bomb'
    def ability(self, player, targets):
        if player.items[self.name]['value'] <= 0:
            print('You don\'t have any left! find or buy some more')
            return player.turn(False)
        player.items[self.name]['value'] -= 1
        for enemy in targets:
            enemy.health -= 3
            print(f'Your bomb dealt 5 damage to {enemy.display_name}')

class Antidote(PlayerTarget):
    name = 'antidote'
    display_name = 'Antidote'
    def ability(self, player, target):
        if player.items[self.name]['value'] <= 0:
            print('You don\'t have any left! find or buy some more')
            return player.turn(False)
        player.items[self.name]['value'] -= 1
        try:
            del target.status_effects['poison']
        except AttributeError:
            pass
        print('You have been cured of poison')
        
