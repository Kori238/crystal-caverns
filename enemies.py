from enemy_abilities import EnemyAttack, EnemyPoison, EnemyCharge, EnemyVulnerableCharged, EnemyGroupHeal, EnemySmashCharged
from status_effects import StatusEffects
from random import randint
from copy import deepcopy
class Enemy(StatusEffects):
    abilities = deepcopy(dict())
    charged = False
    charged_ability = None
    valid = True
    name = display_name = attack = health = max_health = ''
    def __init__(self, pos):
        self.pos = pos
        self.status_effects = deepcopy(dict())
    def turn(self, player):
        to_delete = []
        for key in self.status_effects:
            exec(f'self.{key}(player, False)')
            self.status_effects[key] -= 1
            if self.status_effects[key] == 0:
                to_delete.append(key)
        for item in to_delete:
            del self.status_effects[item]
        if self.charged:
            ability = self.charged_ability
        else:
            abilities = self.abilities
            chance = randint(1,100)
            cumulative_val = 0
            for key, val in abilities.items():
                cumulative_val += val
                if chance > cumulative_val:
                    pass
                else:
                    ability = key
                    break
        print(ability)
        exec('self.'+ability+'(player, self)')
    def death(self, player):
        i = 0
        for enemy in player.enemies:
            if enemy == self:
                player.enemies[i] = None
                break
            i += 1
    def display(self):
        fill = ' '
        allign = '<'
        width = '40'
        message = f'{self.pos+1}: {self.display_name}'
        line1 = f'{message:{fill}{allign}{width}}'
        message = f'Health {self.health}/{self.max_health}'
        line2 = f'{message:{fill}{allign}{width}}'
        if len(self.status_effects.items()) >= 1:
            message = 'Status Effects:'
            line3 = ''
            for status_effect, length in self.status_effects.items():
                message += f'{status_effect}: {str(length)}'
            line3 += f'{message:{fill}{allign}{width}}'
        else:
            message = ''
            line3 = f'{message:{fill}{allign}{width}}'
        return line1, line2, line3


class GreenSnake(Enemy, EnemyAttack):
    name = 'greensnake'
    display_name = 'Green Snake'
    attack = 2
    health = max_health = 5
    abilities = {
        'eattack': 100,
    }

class Spider(Enemy, EnemyAttack, EnemyPoison):
    name = 'spider'
    display_name = 'Spider'
    attack = 1
    health = max_health = 4
    abilities = {
        'eattack': 50,
        'epoison': 50,
    }

class Skeleton(Enemy, EnemyAttack, EnemyCharge, EnemyVulnerableCharged):
    name = 'skeleton'
    display_name = 'Skeleton'
    charged_ability = 'evulnerablecharged'
    attack = 3
    health = max_health = 7
    abilities = {
        'eattack': 50,
        'echarge': 50,
    }

class LivingRock(Enemy, EnemyAttack):
    name = 'livingrock'
    display_name = 'Living Rock'
    attack = 2
    health = max_health = 10
    abilities = {
        'eattack': 100,
    }

class LivingRockGolem(Enemy, EnemyAttack, EnemyGroupHeal, EnemySmashCharged, EnemyCharge):
    name = 'livingrock'
    display_name = 'Living Rock Golem'
    attack = 4
    charged_ability = 'esmashcharged'
    health = max_health = 30
    abilities = {
        'esummonlivingrocks': 15,
        'egroupheal': 15,
        'echarge': 30,
        'eattack': 40,
    }
    def esummonlivingrocks(self, player, enemy):
        print('The Living Rock Golem shapes rocks around it to create Living Rocks')
        if player.enemies[0] == None:
            player.enemies[0] = LivingRock(0)
        if player.enemies[1] == None:
            player.enemies[1] = LivingRock(1)

