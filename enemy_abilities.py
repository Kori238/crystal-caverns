class EnemyAttack:
    def eattack(self, player, enemy):
        if player.health - enemy.attack*player.damage_taken_multiplier <=0:
            print(f'You were hit by {enemy.display_name} for {player.health} damage and fainted!')
            player.health = 0
            player.death()
        else:
            print(f'You were hit by {enemy.display_name} for {str(enemy.attack*player.damage_taken_multiplier)} damage')
            player.health -= enemy.attack * player.damage_taken_multiplier

class EnemyPoison:
    def epoison(self, player, enemy):
        if player.health - enemy.attack/2*player.damage_taken_multiplier <=0:
            print(f'You were hit by {enemy.display_name}\'s poison for {str(int(enemy.attack/2))} damage and fainted!')
            player.health = 0
            player.death()
        else:
            print(f'You were hit by {enemy.display_name} for {str(int(enemy.attack/2*player.damage_taken_multiplier))} damage and got poisoned for 2 turns.')
            player.health -= int(enemy.attack/2*player.damage_taken_multiplier)
            player.status_effects['poisoned'] = 2

class EnemyCharge:
    def echarge(self, player, enemy):
        enemy.charged = True
        print(f'{enemy.display_name} is charging an attack!')

class EnemyVulnerableCharged:
    def evulnerablecharged(self, player, enemy):
        enemy.charged = False
        if player.health - enemy.attack*player.damage_taken_multiplier <= 0:
            print(f'You were hit by {enemy.display_name} for {player.health} damage and fainted!')
            player.health = 0
            player.death()
        else:
            print(f'You were hit by {enemy.display_name} for {enemy.attack*player.damage_taken_multiplier} damage and you are now vulnerable for 1 turn')
            player.health -= enemy.attack*player.damage_taken_multiplier
            player.status_effects['vulnerable'] = 1

class EnemySmashCharged:
    def esmashcharged(self, player, enemy):
        enemy.charged = False
        if player.health - enemy.attack*player.damage_taken_multiplier <= 0:
            print(f'You were hit by {enemy.display_name} for {player.health} damage and fainted!')
            player.health = 0
            player.death()
        else:
            print(f'You were hit by {enemy.display_name} for {enemy.attack*player.damage_taken_multiplier} damage and you are now vulnerable for 2 turns')
            player.health -= enemy.attack*player.damage_taken_multiplier
            player.status_effects['vulnerable'] = 2
        
class EnemyGroupHeal:
    def egroupheal(self, player, enemy):
        print(f'{enemy.display_name} uses the warmth within it to heal itself and its allies')
        for ally in player.enemies:
            if ally.health + 2 >= ally.max_health:
                print(f'{enemy.display_name} healed {ally.display_name} for {ally.max_health - ally.health} health')
                ally.health = ally.max_health
            else:
                print(f'{enemy.display_name} healed {ally.display_name} for 2 health')
                ally.health += 2



