from commands import Info
class SingleTarget():
    def ability(self, player, target):
        pass
    def use(self, player, enemies):
        info = Info(player.weapon.name)
        print('What would you like to target?')
        message = target = ''
        targets = ['b']
        fill = ' '
        allign = '<'
        width = '40'
        try:
            if enemies[0].valid:
                temp = f'(1) {enemies[0].display_name}'
                message += (f'{temp:{fill}{allign}{width}}')
                targets.append('1')
        except AttributeError:
            pass
        try:
            if enemies[1].valid:
                temp = f'(2) {enemies[1].display_name}'
                message += (f'{temp:{fill}{allign}{width}}')
                targets.append('2')
        except AttributeError:
            pass
        try:
            if enemies[2].valid:
                temp = f'(3) {enemies[2].display_name}'
                message += (f'{temp:{fill}{allign}{width}}')
                targets.append('3')
        except AttributeError:
            pass
        temp = '(B) Back'
        message += (f'{temp:{fill}{allign}{width}}')
        while target not in targets:
            info.game_input(target, False, player)
            target = input(message + '\n>').lower()
        if target == 'b':
            return player.turn(False)
        target = int(target)
        if target == 1:
            target = enemies[0]
        elif target == 2:
            target = enemies[1]
        else:
            target = enemies[2]
        self.ability(player, target)

class PlayerTarget():
    def ability(self, player, target):
        pass
    def use(self, player, enemies):
        print('')
        self.ability(player, player)

class MultiTarget():
    def ability(self, player, targets):
        pass
    def use(self, player, enemies):
        targets = []
        try:
            if enemies[0].valid:
                targets.append(enemies[0])
        except AttributeError:
            pass
        try:
            if enemies[1].valid:
                targets.append(enemies[1])
        except AttributeError:
            pass
        try:
            if enemies[2].valid: 
                targets.append(enemies[2])
        except AttributeError:
            pass
        self.ability(player, targets)

class SpecialTarget():
    def ability(self, player, enemies, targets):
        pass
    def use(self, player, enemies):
        targets = []
        try:
            if enemies[0].valid:
                targets.append('1')
        except AttributeError:
            pass
        try:
            if enemies[1].valid:
                targets.append('2')
        except AttributeError:
            pass
        try:
            if enemies[2].valid: 
                targets.append('3')
        except AttributeError:
            pass
        self.ability(player, enemies, targets)