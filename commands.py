from enemies import GreenSnake, Spider, Skeleton, LivingRock, LivingRockGolem
from random import choice
from sys import exit
class Info:
    info_aliases = {
        'snake': 'greensnake',
        'greensnake': 'greensnake',
        'house': 'house',
        'home': 'house',
        'myhouse': 'house',
        'myhome': 'house',
        'cabin': 'house',
        'mycabin': 'house',
        'poison': 'poison',
        'poisoned': 'poison',
        'regen': 'regen',
        'regeneration': 'regen',
        'red': 'red',
        'redgem': 'red',
        'pink': 'pink',
        'pinkgem': 'pink',
        'purple': 'purple',
        'purplegem': 'purple',
        'vulnerable': 'vulnerable',
        'vulnerability': 'vulnerable',
        'stave': 'stave',
        'sword': 'sword',
        'glassstave': 'stave',
        'glasssword': 'sword',
        'theglassstave': 'stave',
        'theglasssword': 'sword',
        'lime': 'lime',
        'limegem': 'lime',
        'me': 'self',
        'self': 'self',
        'player': 'self',
        'mediumhealthpotion': 'mediumhealthpotion',
        'smallhealthpotion': 'smallhealthpotion',
        'largehealthpotion': 'largehealthpotion',
        'mediummagicpotion': 'mediummagicpotion',
        'smallmagicpotion': 'smallmagicpotion',
        'largemagicpotion': 'largemagicpotion',
        'antidote': 'antidote',
        'bomb': 'bomb',
        'skeleton': 'skeleton',
        'damage': 'damage',
        'gemdamage': 'gemdamage',
        'health': 'health',
        'magic': 'magic',
        'livingrock': 'livingrock',
        'livingrockgolem': 'livingrockgolem',
    }
    def __init__(self, weapon):
        self.info_aliases['weapon'] = weapon
    def game_input(self,string,shouldreply,player):
        string = string.lower().replace(' ', '')
        if string[:4] == 'info':
            try:
                exec(f'self.info_{self.info_aliases[string[4:]]}(player)')
            except:
                if shouldreply:
                    print('Sorry I don\'t understand that')
        elif string[:4] == 'help':
            print('''Command words are:
help - Display's this menu
go <place> - Moves the player to the specified place (cannot be done in combat)
info <anything> - Learn more about the specified thing (try anything!) (can be done in combat)
info me - Learn more about your current state such as your health, magic or gold
buy <item> - Buys the specified item (can only be used in shops)''')

    def info_livingrockgolem(self, player):
        print('The boss of the crystal caverns. A very large mass of rocks and gems with a molten center')
        print('It can summon Living Rocks, Charge an attack to deal massive damage and inflict vulnerability on you, Heal itself and it\'s allies or regularly attack')
        print('After you beat this, you beat the game!')

    def info_skeleton(self, player):
        print('A Skeleton is an undead adventurer that tried before you. It can charge an attack to inflict vulnerability on you or it can attack')

    def info_livingrock(self, player):
        print('A Living Rock is a rock reanimated with the energies of the Crystal Cavern. It has 10 health and 2 damage and can only attack.')

    def info_damage(self, player):
        print('Damage increases how much damage you do when you attack')

    def info_gemdamage(self, player):
        print('Gem Damage increases how much damage your gem-related abilities do')

    def info_health(self, player):
        print('Health is a measure of how much damage you can take before you die. When this reaches 0, your game is over')

    def info_magic(self, player):
        print('Magic is the resource used to harness the power of Gems')

    def info_vulnerable(self, player):
        percent = '%'
        print(f'Vulnerability is a debuff that causes the target to take 50{percent} more damage from direct attacks')

    def info_greensnake(self, player):
        print('A Green Snake is a common enemy found in the forest area. It has 5 health and can attack for 2 damage')

    def info_spider(self, player):
        print('A Spider is an enemy found in the webbed caves. It has 4 health and can attack')

    def info_self(self, player):
        player.display()

    def info_house(self,player):
        print('A small little cabin just in the outskirts of Dresdor, surrounded by a dense forest')

    def info_poison(self,player):
        percent = '%'
        print(f'Poison is a debuff that causes the target to lose 10{percent} of their life at the start of their turn')

    def info_regen(self,player):
        percent = '%'
        print(f'Regeneration is a buff that causes the target to heal 10{percent} of their life at the start of their turn')

    def info_stave(self,player):
        print('The Glass Stave. This weapon has a small container to hold gems on the top, allowing you to use them in combat.\nIt increases your magic by 5 and gem damage by 1')

    def info_sword(self,player):
        print('The Glass Sword. This weapon has a small container to hold gems in the hilt, allowing you to use them in combat.\nIt increases your health by 5 and damage by 1')

    def info_lime(self,player):
        print('A chunk of Lime Gem, your weapon can use this to inflict poison on the enemy\nCosts 4 magic, deals damage equal to your gem damage and inflicts poison for 3 turns')

    def info_pink(self,player):
        print('A chunk of Pink Gem, your weapon can use this to apply regeneration to yourself\nCosts 5 magic, applies regen for 3 turns')

    def info_red(self,player):
        percent = '%'
        print(f'A chunk of Red Gem, your weapon can use this to heal you for 33{percent} of your health\nCosts 3 magic')

    def info_purple(self,player):
        print('A chunk of Purple Gem, your weapon can use this to deal deal 2 damage for every point of gem damage you have. This Gem will sometimes malfunction..\nCosts 6 magic')
    
    def info_mediumhealthpotion(self,player):
        print('A glass vial filled with a red potion. You can consume this to restore 10 health')

    def info_largehealthpotion(self,player):
        print('A large glass vial filled with a red potion. You can consume this to restore 20 health')

    def info_smallhealthpotion(self,player):
        print('A small glass vial filled with a red potion. You can consume this to restore 5 health')

    def info_mediummagicpotion(self,player):
        print('A glass vial filled with a blue potion. You can consume this to restore 10 magic')

    def info_largemagicpotion(self,player):
        print('A large glass vial filled with a blue potion. You can consume this to restore 20 magic')

    def info_smallmagicpotion(self,player):
        print('A small glass vial filled with a blue potion. You can consume this to restore 5 magic')

    def info_antidote(self,player):
        print('A vial filled with green liquid. You can consume this to cure all your debuffs')

    def info_bomb(self,player):
        print('A black ball filled with explosives. You can use this to deal 5 damage to every enemy in combat with you')

class Go:
    place_aliases = {
        'thevillage': 'village',
        'theforest': 'forest',
        'village': 'village',
        'forest': 'forest',
        'outside': 'outside',
        'cabin': 'outside',
        'mycabin': 'outside',
        'deeper': 'deeper',
        'farther': 'deeper',
        'further': 'deeper',
        'on': 'deeper',
        'back': 'outside',
        'left': 'left',
        'right': 'right',
        'shop': 'shop',
    }
    def __init__(self):
        pass
    def game_input(self,string,shouldreply,player):
        player.next_position = ''
        string = string.lower().replace(' ', '')
        if string[:4] == 'goto':
            try:
                exec(f'self.go_{self.place_aliases[string[4:]]}(player)')
            except:
                if shouldreply:
                    print('Sorry I don\'t understand that')
        elif string[:2] == 'go':
            try:
                exec(f'self.go_{self.place_aliases[string[2:]]}(player)')
            except:
                if shouldreply:
                    print('Sorry I don\'t understand that')
        elif string[:3] == 'buy':
            if player.position != 'shop':
                print('You must be in the shop to buy something...')
            else:
                try:
                    exec(f'self.buy_{string[3:]}(player)')
                except:
                    if shouldreply:
                        print('Sorry I don\'t understand that')
        elif string[:5] == 'leave':
            self.go_outside(player)

    def go_outside(self, player):
        if player.position == 'shop':
            print('You leave the shop and go back to the village')
            print('You can go to the shop or back to your cabin')
            player.position = 'village'

        elif player.position == 'cabin' and player.canleave:
            print('\nYou open the door to your cabin and step outside, there is forest all around you and a small village to your right')
            print('You can go to the forest or the village')
            player.position = 'outside cabin'

        elif player.canleave:
            print('\nYou walk back to your cabin')
            print('you can go to the forest or to the small village')
            player.position = 'outside cabin'
        else:
            try:
                if player.position[7] == 'R':
                    print('You walk back to the fork, there is a sign pointing to the left')
                    print('You can go left, right or back to your cabin')
                    player.position = 'forest6'
            except:
                pass

    def go_forest(self, player):
        print(player.farthest)
        if player.farthest[:6] == 'forest':
            self.go_deeper(player)
        elif player.position == 'outside cabin':
            print('\nYou step into the forest, you know the enterance to the Crystal Caverns is around here somewhere, you just have to look a bit more')
            print('You can go deeper or back to your cabin')
            player.position = player.farthest = 'forest'

    def go_deeper(self, player):
        if True == False:
            pass
        elif player.position == 'forestR5':
            print('Well done! You\'ve beaten the Crystal Caverns!')
            print('Thank you for playing :)')
            exit()

        elif player.position == 'forestR4':
            print('You can see light! Is this the legend of the Crystal Caverns?')
            print('You see something in the ground shift beneath you')
            print('A large mass of stone awakens with a molten glowing center.. It doesnt seem very happy about your presence')
            print('Some nearby skeletons seem to want to join in..')
            player.enemies[0] = Skeleton(0)
            player.enemies[1] = LivingRockGolem(1)
            player.enemies[2] = Skeleton(2)
            player.combat = True
            player.position = 'forestR5'
            player.next_position = 'go deeper'

        elif player.position == 'forestR3':
            player.gold += 320
            print('You gained 320 Gold')
            print('There seems to be light eminating from the room ahead of you.. perhaps you should prepare before heading forwards')
            print('You can go deeper or back to your cabin')
            player.position = player.farthestR = 'forestR4'

        elif player.position == 'forestR2':
            print('You can see some light up ahead but some weird looking rocks and a skeleton block your path')
            player.enemies[0] = LivingRock(0)
            player.enemies[1] = Skeleton(1)
            player.enemies[2] = LivingRock(0)
            player.combat = True
            player.position = player.farthestR = 'forestR3'
            player.next_position = 'go deeper'

        elif player.position == 'forestR1':
            print('Well done! You beat the skeleton')
            player.gold += 100
            print('You gained 100 Gold')
            player.position = player.farthestR = 'forestR2'
            print('You can go deeper or back to your cabin')

        elif player.position == 'forestR':
            print('Lurking in the shadows a Skeleton appears!')
            player.enemies[0] = Skeleton(0)
            player.combat = True
            player.position = player.farthestR = 'forestR1'
            player.next_position = 'go deeper'

        elif player.position == 'forestL2':
            print('You emerge from a small passage above where you came, after a small drop you find yourself back to the fork you were at earlier')
            print('There is a sign pointing to the left')
            player.position = 'forest6'
            player.farthest = 'forest5'
            print('You can go left, right or back')
            player.canleave = True

        elif player.position == 'forestL1':
            print('You gained 90 gold')
            player.gold += 90
            print('After defeating the spiders you make your way through the rest of the passage. You emerge in a room with some loose Lime Gem in the center, you collect it')
            print('You can see another passage that looks like it leads back in the direction you came.. Perhaps you can get back through this?')
            print('You can go deeper')
            player.addgems = ['lime']
            player.position = player.farthestL = 'forestL2'

        elif player.position == 'forestL':
            print('Entering the webbed passage probably wasnt a good idea, you can hear spiders hissing all around you. It was only inevitable that they would try and eat you')
            player.enemies[0] = Spider(0)
            player.enemies[1] = Spider(1)
            player.enemies[2] = Spider(2)
            print('You are surrounded by spiders! It seems like the only way through is by fighting.')
            player.combat = True
            player.position = player.farthestL = 'forestL1'
            player.next_position = 'go deeper'
            player.canleave = False

        elif player.position == 'forest5' or player.farthest == 'forest5':
            print('As soon as you enter the Crystal Caverns you\'re met with a fork in your path, There is an arrow in the center pointing to the left.')
            print('You can go left, right, or back to your cabin')
            player.position = 'forest6'

        elif player.position == 'forest4' or player.farthest == 'forest4':
            print('A large stone cave stands before you in a small clearing in the forest. It is embedded with gems of various colours and many warning signs')
            print(f'It seems a chunk of Red Gem is loose, you decide to collect it')
            player.addgems = ['red']
            player.position = player.farthest = 'forest5'
            print('\nYou can go deeper into the depths of the Crystal Caverns or go back to your cabin')

        elif player.position == 'forest3' or player.farthest == 'forest3':
            print('You gained 40 gold')
            player.gold += 40
            print('\nYou realise getting to and through the depths of the Crystal Cavern is going to be harder than you thought')
            print('You can go deeper or back to your cabin')
            player.position = player.farthest = 'forest4'
            player.next_position = ''

        elif player.position == 'forest2' or player.farthest == 'forest2':
            print('It seems the snake\'s friends werent very happy about you killing him...')
            player.enemies[0] = GreenSnake(0)
            player.enemies[1] = GreenSnake(1)
            player.combat = True
            player.position = player.farthest = 'forest3'
            player.next_position = 'go deeper'

        elif player.position == 'forest1' or player.farthest == 'forest1':
            print('You gained 20 gold')
            player.gold += 20
            print('\nWell done! you beat the snake!')
            print('You notice some Purple Gem scattered on the ground, you decide to collect some')
            player.addgems.append('purple')
            print('You can go deeper or back to your cabin')
            player.position = player.farthest = 'forest2'

        elif player.position == 'forest':
            player.enemies[0] = GreenSnake(0)
            print('\nWhile wandering deeper into the forest, you encounter a Green Snake! It\'s blocking your path.')
            player.combat = True
            player.position = player.farthest = 'forest1'
            player.next_position = 'go deeper'

    def go_left(self, player):
        if player.position == 'forest6' and player.farthestL == 'forestL2':
            print('You go in a loop and find yourself back in the room you left..')
            print('You can go left, right or back')
            player.position = 'forest6'

        elif player.position == 'forest6':
            print('The ground caves beneath you! You fall a small distance and look back up to where you entered this room. There is no way back up!')
            print('You can see a small webbed passage. It looks dangerous but you don\'t seem to have any other choice..')
            print('You can go deeper')
            player.position = player.farthestL = 'forestL'
            player.canleave = False

    def go_right(self, player):
        try:
            player.position = player.farthestR
            self.go_deeper(player)
        except:
            pass
        if player.position == 'forest6':
            print('As you walk through the cave you a vein of Pink Gem, you decide to collect some')
            player.addgems.append('pink')
            print('You can go back or go deeper')
            player.position = 'forestR'

    def go_village(self, player):
        print('There are many houses in this village but there is only one shop')
        print('You can go home or you can go to the shop')
        player.position = 'village'

    def go_shop(self, player):
        print('The merchant has some goods for sale')
        print('Small Health Potion: 25 Gold            Medium Health Potion: 50 Gold            Large Health Potion: 75 Gold')
        print('Small Magic Potion: 25 Gold             Medium Magic Potion: 50 Gold             Large Magic Potion: 75 Gold')
        print('Antidote: 15 Gold                       Bomb: 100 Gold                           Max Health(+5): 100 Gold')
        print('Max Magic (+5): 100 Gold                Damage(+1): 100 Gold                     Gem Damage(+1): 100 Gold')

        print(f'You have {str(player.gold)} Gold')
        print('You can leave or buy something (buy <item name>)')
        player.position = 'shop'

    def buy_smallhealthpotion(self, player):
        if player.gold < 25:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('smallhealthpotion')
            player.gold -= 25
            print('You bought a Small Health Potion')

    def buy_mediumhealthpotion(self, player):
        if player.gold < 50:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('mediumhealthpotion')
            player.gold -= 50
            print('You bought a Medium Health Potion')

    def buy_largehealthpotion(self, player):
        if player.gold < 75:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('largehealthpotion')
            player.gold -= 75
            print('You bought a Large Health Potion')

    def buy_smallmagicpotion(self, player):
        if player.gold < 25:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('smallmagicpotion')
            player.gold -= 25
            print('You bought a Small Magic Potion')

    def buy_mediummagicpotion(self, player):
        if player.gold < 50:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('mediummagicpotion')
            player.gold -= 50
            print('You bought a Medium Magic Potion')

    def buy_largemagicpotion(self, player):
        if player.gold < 75:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('largemagicpotion')
            player.gold -= 75
            print('You bought a Large Magic Potion')

    def buy_antidote(self, player):
        if player.gold < 15:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('antidote')
            player.gold -= 15
            print('You bought an Antidote')

    def buy_bomb(self, player):
        if player.gold < 100:
            print('You don\'t have enough gold for this')
        else:
            player.additems.append('bomb')
            player.gold -= 100
            print('You bought a Bomb')

    def buy_maxhealth(self, player):
        if player.gold < 100:
            print('You don\'t have enough gold for this')
        else:
            player.gold -= 100
            player.max_health += 5
            player.health += 5
            print('Your maximum health has increased by 5!')

    def buy_maxmagic(self, player):
        if player.gold < 100:
            print('You don\'t have enough gold for this')
        else:
            player.gold -= 100
            player.max_magic += 5
            player.magic += 5
            print('Your maximum magic has increased by 5!')

    def buy_damage(self, player):
        if player.gold < 125:
            print('You don\'t have enough gold for this')
        else:
            player.gold -= 125
            player.damage += 1
            print('Your damage has increased by 1')

    def buy_gemdamage(self, player):
        if player.gold < 125:
            print('You don\'t have enough gold for this')
        else:
            player.gold -= 125
            player.gem_damage += 1
            print('Your gem damage has increased by 1')